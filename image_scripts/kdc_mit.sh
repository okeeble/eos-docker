#!/usr/bin/env bash

# Install MIT Kerberos server
yum install -y krb5-server

# Initialize the KDC database
kdb5_util create -r TEST.EOS -s -P testeos

# Add Kerberos entities
kadmin.local -r TEST.EOS add_principal -randkey -maxlife 0 -maxrenewlife 0 admin1
kadmin.local -r TEST.EOS add_principal -randkey -maxlife 0 -maxrenewlife 0 host/eos-mgm1.eoscluster.cern.ch

# Generate keytab files
kadmin.local -r TEST.EOS ktadd -k /root/admin1.keytab admin1
kadmin.local -r TEST.EOS ktadd -k /root/eos.keytab host/eos-mgm1.eoscluster.cern.ch

# Start the kdc
/usr/sbin/krb5kdc -r TEST.EOS

