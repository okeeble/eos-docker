#!/usr/bin/env bash

# Preload jemalloc
jemalloc=$(find /usr/lib64/ -name libjemalloc* | sort)

if [[ ! -z "${jemalloc}" ]]; then
  export LD_PRELOAD=${jemalloc}
fi

quarkdb-create --path /var/quarkdb/node-0
chown -R daemon:daemon /var/quarkdb/node-0
/usr/bin/xrootd -n qdb -c /etc/xrd.cf.quarkdb -l /var/log/eos/xrdlog.qdb -b -Rdaemon
