#
# Simple EOS Docker file
#
# Version 0.2

FROM centos:7
MAINTAINER Elvin Sindrilaru, esindril@cern.ch, CERN 2017

# Add extra repositories
ADD eos-docker/el-7/*.repo /etc/yum.repos.d/

# Add helper scripts
ADD eos-docker/image_scripts/*.sh /

# Add configuration files for EOS instance
ADD eos-docker/eos.sysconfig /etc/sysconfig/eos
ADD eos-docker/xrd.cf.* eos-docker/krb5.conf /etc/
ADD eos-docker/fuse.conf /etc/eos/fuse.mount-1.conf
ADD eos-docker/fuse.conf /etc/eos/fuse.mount-2.conf
ADD eos-docker/fstfmd.dict /var/eos/md/

RUN mkdir /var/tmp/eosxd-cache/ /var/tmp/eosxd-journal/
RUN useradd eos-user

# Docker will aggressively cache the following command, but this is fine, since
# these packages are not updated often.
RUN yum -y --nogpg install \
    heimdal-server heimdal-workstation \
    krb5-workstation yum-plugin-priorities \
    createrepo initscripts less nano \
    git parallel compat-libf2c-34 libgfortran \
    gdb gcc-c++ cmake3 libacl-devel perl-Test-Harness \
    rpm-build bzip2 automake autoconf libtool sudo vim at

# Special packages, must be installed un-cached.
RUN yum clean all; \
    yum -y --nogpg --enablerepo=xrootd-testing --setopt="eos-depend.exclude=xrootd*" install \
    quarkdb grid-hammer davix
    
# Install new EOS from created repo - the ADD command will reset the docker cache,
# and any commands after that point will be uncached.
ENV EOSREPODIR="/repo/eos"
ADD cc7_xrd_testing_artifacts ${EOSREPODIR}

RUN createrepo ${EOSREPODIR}; \
    echo -e "[eos-artifacts]\nname=EOS artifacts\nbaseurl=file://${EOSREPODIR}\ngpgcheck=0\nenabled=1\npriority=1" >> /etc/yum.repos.d/eos.repo; \
    yum -y --nogpg --enablerepo=xrootd-testing --setopt="eos-depend.exclude=xrootd*,python2-xrootd*" install \
    eos-server eos-testkeytab eos-archive eos-client \
    eos-debuginfo eos-fuse eos-fusex eos-test eos-ns-inspect

# Generate a new forwardable keytab 'eos-test+' to replace the not-forwardable one (installed by the eos-testkeytab package).
# This is useful to deploy EOS on Kubernetes clusters running on CERN's Cloud Infrastructure; you can remove these lines if you don't need one.
RUN yes | xrdsssadmin -k eos-test del /etc/eos.keytab; \
    yes | xrdsssadmin -u daemon -g daemon -k eos-test+ -n 1234567890123456789 add /etc/eos.keytab

# Setup keytab permissions
RUN chown daemon:daemon /etc/eos.keytab; \
    chmod 400 /etc/eos.keytab

ENTRYPOINT ["/bin/bash"]
